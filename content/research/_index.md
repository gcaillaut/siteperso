+++
title = "Recherche"

[extra]
hide_title = false
+++

Mes travaux de recherche gravitent principalement autour de l'apprentissage d'espaces prétopologiques et de ses *nombreuses* applications.

## Publications

*Learning pretopological spaces to extract ego-centered communities*  
Gaëtan Caillaut, Guillaume Cleuziou, Nicolas Dugué  
**PAKDD 2019**  
[{{ trans(key="cite", lang="fr") }}](https://dblp.org/rec/conf/pakdd/CaillautCD19) [{{ trans(key="download", lang="fr") }}](/pdf/pakdd2019.pdf)

*Extraction de communautés ego-centrées par apprentissage supervisé d'espaces prétopologiques*  
Gaëtan Caillaut, Guillaume Cleuziou, Nicolas Dugué  
**EGC 2019**  
[{{ trans(key="cite", lang="fr") }}](https://dblp.org/rec/conf/f-egc/CaillautCD19) [{{ trans(key="download", lang="fr") }}](/pdf/egc2019.pdf)

*Apprentissage d’espaces prétopologiques dans un cadre multi-instance pour la structuration de données*  
Gaëtan Caillaut, Guillaume Cleuziou  
**EGC 2017**  
[{{ trans(key="cite", lang="fr") }}](https://dblp.org/rec/conf/f-egc/CaillautC17) [{{ trans(key="download", lang="fr") }}](/pdf/egc2017.pdf)