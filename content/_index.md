+++
title = "Accueil"
+++

Bienvenue sur mon site. Je suis Gaëtan Caillaut et suis actuellement Attaché Temporaire d'Enseignement et de Recherche (ATER) à l'[université d'Orléans](https://www.univ-orleans.fr/fr).

J'effectue mes missions d'enseignement au [département informatique](https://www.univ-orleans.fr/fr/iut-orleans/formation/informatique) de l'IUT d'Orléans et mes recherches au [Laboratoire d'Informatique Fondamentale d'Orléans (LIFO)](https://www.univ-orleans.fr/lifo).

Ma thèse, dirigée par [Guillaume Cleuziou](https://sites.google.com/site/guillaumecleuzioulifo/), s'intitule <q>Apprentissage d'espaces prétopologiques pour l'extraction de connaissances structurées</q>.
