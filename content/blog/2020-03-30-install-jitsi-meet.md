+++
title="Installer Jitsi-Meet sur un serveur Archlinux"
draft=false

[extra]

[taxonomies]
tags=["jitsi-meet"]
+++

L'autre jour, j'ai voulu héberger une instance de [Jitsi-Meet](https://github.com/jitsi/jitsi-meet), un logiciel libre de vidéconférence, sur mon serveur tournant sous [Archlinux](https://www.archlinux.org/). Malheureusement, la documentation officielle de Jitsi-Meet couvre uniquement l'installation sur une distribution dérivée de Debian et je ne suis pas parvenu à avoir une installation fonctionnelle à partir de paquets disponible sur le AUR.

Ce guide, basé sur la [documentation officielle](https://github.com/jitsi/jitsi-meet/blob/master/doc/manual-install.md), à pour objectif de décrire chaque étape menant à l'installation de Jitsi-Meet sur Archlinux, mais il devrait globalement convenir pour tout autre distribution.

<!-- more -->

Jitsi-Meet n'est *que* l'interface autour du logiciel Jitsi. Il est nécessaire d'installer un certain nombre de logiciel pour espérer disposer d'une installation fonctionelle, à savoir :

- [Jitsi-Meet](https://github.com/jitsi/jitsi-meet), évidemment
- [Jitsi-Videobridge](https://github.com/jitsi/jitsi-videobridge)
- [JItsi COnference FOcus (Jicofo)](https://github.com/jitsi/jicofo) 
- Un serveur XMPP, j'utilise [Prosody](https://prosody.im/) mais j'imagine qu'il est possible d'en utiliser un autre
- Un serveur HTTP, j'utilise [Nginx](https://nginx.org/)

Chacun de ces logiciels communiquent comme indiqué ci-dessous. Les nombres indiquent les ports utilisés.

```
                   +                           +
                   |                           |
                   |                           |
                   v                           |
                  443                          |
               +-------+                       |
               |       |                       |
               | Nginx |                       |
               |       |                       |
               +--+-+--+                       |
                  | |                          |
+------------+    | |    +--------------+      |
|            |    | |    |              |      |
| jitsi-meet +<---+ +--->+ prosody/xmpp |      |
|            |files 5280 |              |      |
+------------+           +--------------+      v
                     5222,5347^    ^5347   4443,10000
                +--------+    |    |    +-------------+
                |        |    |    |    |             |
                | jicofo +----^    ^----+ videobridge |
                |        |              |             |
                +--------+              +-------------+
```

On va commencer par installer chacun de ces logiciels.

## Prosody

## Installation

Prosody est disponible sur les dépôts d'archlinux. Attention à version de lua utilisée : la bibliothèque lua-sec semble mal fonctionner avec Prosody avec les versions de Lua autres que 5.2.

```sh
sudo pacman -S prosody lua52-sec cyrus-sasl
```

Il faut penser, si ça n'a pas été fait au cours de l'installation, à modifier les permisisons du dossier `/var/lib/prosody`.

```sh
sudo chown -R prosody:prosody /var/lib/prosody
```

## Configuration

Mon fichier `/etc/prosody/prosody.cfg.lua` est donné ci-dessous.

```lua
--Important for systemd
-- daemonize is important for systemd. if you set this to false the systemd startup will freeze.
daemonize = true
pidfile = "/run/prosody/prosody.pid"

-- Prosody Example Configuration File
--
-- Information on configuring Prosody can be found on our
-- website at https://prosody.im/doc/configure
--
-- Tip: You can check that the syntax of this file is correct
-- when you have finished by running this command:
--     prosodyctl check config
-- If there are any errors, it will let you know what and where
-- they are, otherwise it will keep quiet.
--
-- The only thing left to do is rename this file to remove the .dist ending, and fill in the
-- blanks. Good luck, and happy Jabbering!


---------- Server-wide settings ----------
-- Settings in this section apply to the whole server and are the default settings
-- for any virtual hosts

-- This is a (by default, empty) list of accounts that are admins
-- for the server. Note that you must create the accounts separately
-- (see https://prosody.im/doc/creating_accounts for info)
-- Example: admins = { "user1@example.com", "user2@example.net" }
admins = { }

cross_domain_bosh = true;
component_ports = { 5347 }
-- component_interface = "127.0.0.1"

-- Enable use of libevent for better performance under high load
-- For more information see: https://prosody.im/doc/libevent
--use_libevent = true

-- Prosody will always look in its source directory for modules, but
-- this option allows you to specify additional locations where Prosody
-- will look for modules first. For community modules, see https://modules.prosody.im/
--plugin_paths = {}

-- This is the list of modules Prosody will load on startup.
-- It looks for mod_modulename.lua in the plugins folder, so make sure that exists too.
-- Documentation for bundled modules can be found at: https://prosody.im/doc/modules
modules_enabled = {

        -- Generally required
                "roster"; -- Allow users to have a roster. Recommended ;)
                "saslauth"; -- Authentication for clients and servers. Recommended if you want to log in.
                "tls"; -- Add support for secure TLS on c2s/s2s connections
                "dialback"; -- s2s dialback support
                "disco"; -- Service discovery
                "posix"; -- POSIX functionality, sends server to background, enables syslog, etc.

        -- Not essential, but recommended
                "carbons"; -- Keep multiple clients in sync
                "pep"; -- Enables users to publish their avatar, mood, activity, playing music and more
                "private"; -- Private XML storage (for room bookmarks, etc.)
                "blocklist"; -- Allow users to block communications with other users
                "vcard4"; -- User profiles (stored in PEP)
                "vcard_legacy"; -- Conversion between legacy vCard and PEP Avatar, vcard

        -- Nice to have
                "version"; -- Replies to server version requests
                "uptime"; -- Report how long server has been running
                "time"; -- Let others know the time here on this server
                "ping"; -- Replies to XMPP pings with pongs
                "register"; -- Allow users to register on this server using a client and change passwords
                --"mam"; -- Store messages in an archive and allow users to access it
                --"csi_simple"; -- Simple Mobile optimizations

        -- Admin interfaces
                "admin_adhoc"; -- Allows administration via an XMPP client that supports ad-hoc commands
                --"admin_telnet"; -- Opens telnet console interface on localhost port 5582

        -- HTTP modules
                "bosh"; -- Enable BOSH clients, aka "Jabber over HTTP"
                --"websocket"; -- XMPP over WebSockets
                --"http_files"; -- Serve static files from a directory over HTTP

        -- Other specific functionality
                --"limits"; -- Enable bandwidth limiting for XMPP connections
                --"groups"; -- Shared roster support
                --"server_contact_info"; -- Publish contact information for this service
                --"announce"; -- Send announcement to all online users
                --"welcome"; -- Welcome users who register accounts
                --"watchregistrations"; -- Alert admins of registrations
                --"motd"; -- Send a message to users when they log in
                --"legacyauth"; -- Legacy authentication. Only used by some old clients and bots.
                --"proxy65"; -- Enables a file transfer proxy service which clients behind NAT can use

        -- jitsi
                "smacks";
                "carbons";
                "mam";
                "lastactivity";
                "offline";
                "pubsub";
                "adhoc";
                "websocket";
                "http_altconnect";
}

-- These modules are auto-loaded, but should you want
-- to disable them then uncomment them here:
modules_disabled = {
        -- "offline"; -- Store offline messages
        -- "c2s"; -- Handle client connections
        -- "s2s"; -- Handle server-to-server connections
        -- "posix"; -- POSIX functionality, sends server to background, enables syslog, etc.
}

-- Disable account creation by default, for security
-- For more information see https://prosody.im/doc/creating_accounts
allow_registration = false

-- Force clients to use encrypted connections? This option will
-- prevent clients from authenticating unless they are using encryption.

-- c2s_require_encryption = true

-- Force servers to use encrypted connections? This option will
-- prevent servers from authenticating unless they are using encryption.

-- s2s_require_encryption = true

-- Force certificate authentication for server-to-server connections?

-- s2s_secure_auth = true

-- Some servers have invalid or self-signed certificates. You can list
-- remote domains here that will not be required to authenticate using
-- certificates. They will be authenticated using DNS instead, even
-- when s2s_secure_auth is enabled.

--s2s_insecure_domains = { "insecure.example" }

-- Even if you disable s2s_secure_auth, you can still require valid
-- certificates for some domains by specifying a list here.

--s2s_secure_domains = { "jabber.org" }

-- Select the authentication backend to use. The 'internal' providers
-- use Prosody's configured data storage to store the authentication data.

authentication = "internal_hashed"

-- Select the storage backend to use. By default Prosody uses flat files
-- in its configured data directory, but it also supports more backends
-- through modules. An "sql" backend is included by default, but requires
-- additional dependencies. See https://prosody.im/doc/storage for more info.

--storage = "sql" -- Default is "internal"

-- For the "sql" backend, you can uncomment *one* of the below to configure:
--sql = { driver = "SQLite3", database = "prosody.sqlite" } -- Default. 'database' is the filename.
--sql = { driver = "MySQL", database = "prosody", username = "prosody", password = "secret", host = "localhost" }
--sql = { driver = "PostgreSQL", database = "prosody", username = "prosody", password = "secret", host = "localhost" }


-- Archiving configuration
-- If mod_mam is enabled, Prosody will store a copy of every message. This
-- is used to synchronize conversations between multiple clients, even if
-- they are offline. This setting controls how long Prosody will keep
-- messages in the archive before removing them.

archive_expires_after = "1w" -- Remove archived messages after 1 week

-- You can also configure messages to be stored in-memory only. For more
-- archiving options, see https://prosody.im/doc/modules/mod_mam

-- Logging configuration
-- For advanced logging see https://prosody.im/doc/logging
log = {
        -- info = "prosody.log"; -- Change 'info' to 'debug' for verbose logging
        -- error = "prosody.err";
        "*syslog"; -- Uncomment this for logging to syslog
        -- "*console"; -- Log to the console, useful for debugging with daemonize=false
}

-- Uncomment to enable statistics
-- For more info see https://prosody.im/doc/statistics
-- statistics = "internal"

-- Certificates
-- Every virtual host and component needs a certificate so that clients and
-- servers can securely verify its identity. Prosody will automatically load
-- certificates/keys from the directory specified here.
-- For more information, including how to use 'prosodyctl' to auto-import certificates
-- (from e.g. Let's Encrypt) see https://prosody.im/doc/certificates

-- Location of directory to find certificates in (relative to main config file):
certificates = "certs"

-- HTTPS currently only supports a single certificate, specify it here:
--https_certificate = "/etc/prosody/certs/localhost.crt"

----------- Virtual hosts -----------
-- You need to add a VirtualHost entry for each domain you wish Prosody to serve.
-- Settings under each VirtualHost entry apply *only* to that host.

VirtualHost "jitsi.<DOMAINE>"
    authentication = "anonymous"
    modules_enabled = {
        "bosh";
        "pubsub";
    }
    c2s_require_encryption = false

VirtualHost "auth.jitsi.<DOMAINE>"
    authentication = "internal_plain"

admins = { "focus@auth.jitsi.<DOMAINE>" }

------ Components ------
-- You can specify components to add hosts that provide special services,
-- like multi-user conferences, and transports.
-- For more information on components, see https://prosody.im/doc/components

---Set up a MUC (multi-user chat) room server on conference.example.com:
--Component "conference.example.com" "muc"
--- Store MUC messages in an archive and allow users to access it
--modules_enabled = { "muc_mam" }

---Set up an external component (default component port is 5347)
--
-- External components allow adding various services, such as gateways/
-- transports to other networks like ICQ, MSN and Yahoo. For more info
-- see: https://prosody.im/doc/components#adding_an_external_component
--
--Component "gateway.example.com"
--      component_secret = "password"

Component "conference.jitsi.<DOMAINE>" "muc"

Component "jitsi-videobridge.jitsi.<DOMAINE>"
    component_secret = "<SECRET1>"

Component "focus.jitsi.<DOMAINE>"
    component_secret = "<SECRET2>"
```

Il faut remplacer les `<DOMAINE>`, `<SECRET1>` et `<SECRET2>` par, respectivement, un nom de domaine et deux *secrets* qui serviront à authentifié les différents agents du système. De cette manière, seul le videobridge pourra communiquer avec le composant `jitsi-videobridge.jitsi.<DOMAINE>`.

Enfin, on ajoute un utilisateur, nommé **focus** et on lui attribue le mot de passe `<SECRET3>`.

```sh
prosodyctl register focus auth.jitsi.<DOMAINE> <SECRET3>
```

J'ai utilisé le logiciel *pwgen* pour générer mes trois secrets, tous composés de 30 caractères. Il est disponible sur les dépôts d'Archlinux.

```sh
pwgen 30 3
```

On peut également utiliser openssl :

```sh
openssl rand -base64 30
```

## Certificats SSL

J'ai ensuite généré des certificats SSL pour chacune des URLs mentionnées dans le fichier `/etc/prosody/prosody.cfg.lua`. Pour ce faire, j'utilise [certbot](https://certbot.eff.org/), qui permet de récupérer assez facilement des certificats signés par [Let's Encrypt](https://letsencrypt.org/).

```
sudo pacman -S certbot
```

Le site de [certbot](https://certbot.eff.org/) détaille particulièrement les étapes à suivre pour obtenir un certificat. En ce qui me concerne, j'ai entré, en root, les commandes suivantes (dans mon cas, il a fallu également installer le paquet `certbot-dns-ovh`) :

```sh
certbot certonly --dns-ovh --dns-ovh-credentials ~/.config/.ovh-credentials.txt -d jitsi.<DOMAINE>
certbot certonly --dns-ovh --dns-ovh-credentials ~/.config/.ovh-credentials.txt -d auth.jitsi.<DOMAINE>
```

Mais, dans la majorité des cas, les commandes suivantes devraient suffir (l'option `-d` permet d'indiquer le nom de domaine à certifier) :

```sh
sudo certbot certonly --nginx -d jitsi.<DOMAINE>
sudo certbot certonly --nginx -d auth.jitsi.<DOMAINE>
```

Les certificats sont alors installés dans les dossiers `/etc/letsencrypt/live/jitsi.<DOMAINE>` et `/etc/letsencrypt/live/auth.jitsi.<DOMAINE>`. Cependant, ces dossier ne sont accessible que par **root**, et non pas par **prosody**. Il est nécessaire d'importer les certificat à l'aide de la commande suivante (en **root**) :

```sh
prosodyctl --root cert import /etc/letsencrypt/live
```

La [documentation](https://prosody.im/doc/letsencrypt) de Prosody indique comment exécuter cette commande à chaque renouvellement de certificats :

```
certbot renew --deploy-hook "prosodyctl --root cert import /etc/letsencrypt/live"
```

On peut à présent démarrer Prosody.

```sh
sudo systemctl start prosody.service
```

# Jitsi videobridge

## Installation

Pour commencer, j'ai créer l'utilisateur **jitsi**. C'est cet utilisateur qui exécutera les différents programmes.

```
useradd jitsi -m -d /opt/jitsi -s /usr/bin/nologin
```

J'ai choisi de me reposer sur les binaires précompilés par l'équipe derrière Jitsi. Il faut les télécharger puis les décompresser.

```sh
cd /opt/jitsi
wget https://download.jitsi.org/jitsi-videobridge/linux/jitsi-videobridge-linux-x64-1132.zip
unzip jitsi-videobridge-linux-x64-1132.zip
rm -r jitsi-videobridge-linux-x64-1132.zip
chown -R jitsi:jitsi jitsi-videobridge-linux-x64-1132
```

Puis on crée le fichier `/opt/jitsi/.sip-communicator/sip-communicator.properties`. Ce fichier très important permet au videobridge de communiquer l'adresse du serveur WebRTC à chacun des participants. De plus amples détails sur les différents paramètres modifiables sont donnés [ici](https://github.com/jitsi/ice4j/blob/master/doc/configuration.md).

```sh
org.jitsi.impl.neomedia.transform.srtp.SRTPCryptoContext.checkReplay=false

# The videobridge uses 443 by default with 4443 as a fallback, but since we're already
# running nginx on 443 in this example doc, we specify 4443 manually to avoid a race condition
org.jitsi.videobridge.TCP_HARVESTER_PORT=4443
org.jitsi.videobridge.TCP_HARVESTER_MAPPED_PORT=4443

org.ice4j.ice.harvest.NAT_HARVESTER_LOCAL_ADDRESS=<IP LOCALE>
# L'adresse IP globale peut être récupérer, par exemple, depuis https://duckduckgo.com/?q=my+ip
org.ice4j.ice.harvest.NAT_HARVESTER_PUBLIC_ADDRESS=<IP GLOBALE>

org.ice4j.ice.harvest.DISABLE_AWS_HARVESTER=true
```

Puis on crée le fichier `/etc/jitsi-videobridge/jitsi-videobridge.conf`. Il faut remplacer `<DOMAINE>` et `<SECRET1>` par les même valeurs que dans le fichier de configuration de Prosody.

```
flags="--host=localhost --domain=jitsi.<DOMAINE> --port=5347 --secret=<SECRET1>"
```

Enfin, on crée le fichier *jitsi-videobridge.service* que l'on copie dans `/etc/systemd/system`.

```
[Unit]
Description=Jitsi Videobridge
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
User=jitsi
Group=jitsi
EnvironmentFile=/etc/jitsi-videobridge/jitsi-videobridge.conf
ExecStart=/opt/jitsi/jitsi-videobridge-linux-x64-1132/jvb.sh ${flags}
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

Il est nécessaire de recharger systemd.

```sh
sudo systemctl daemon-reload
sudo systemctl start jitsi-videobridge.service
```

# Jicofo

## Installation

Cette fois, j'ai décidé de compiler Jicofo depuis ses sources.

```sh
cd /opt/jitsi
git clone https://github.com/jitsi/jicofo.git
chown -R jitsi:jitsi jicofo
cd jicofo
sudo -u jitsi mvn package -DskipTests -Dassembly.skipAssembly=false
sudo -u jitsi unzip target/jicofo-1.1-SNAPSHOT-archive.zip
```

## Configuration

Tout comme pour le videobridge, on créer dans un premier temps le fichier de configuration `/etc/jicofo/jicofo.conf`.

```
flags="--host=localhost --domain=jitsi.<DOMAINE> --secret=<SECRET2> --user_domain=auth.jitsi.<DOMAINE> --user_name=focus --user_password=<SECRET3>"
```

Puis on créer le fichier *jicofo.service*, que l'on copie dans `/etc/systemd/system`.

```
[Unit]
Description=Jicofo
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
User=jitsi
Group=jitsi
EnvironmentFile=/etc/jicofo/jicofo.conf
ExecStart=/opt/jitsi/jicofo/jicofo-1.1-SNAPSHOT/jicofo.sh ${flags}
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

On peut alors démarrer le service jicofo.

```sh
sudo systemctl daemon-reload
sudo systemctl start jicofo.service
```

# Jitsi Meet

La dernière étape consiste à installe l'application web : Jitsi-Meet. Dans un premier temps, il est nécessaire d'installe le paquet nginx. L'utilisateur **http** devrait être créer lors de l'installation.

```
pacman -S nginx-mainline
```

## Téléchargement des sources

J'ai récupérer les sources de Jitsi-Meet, que j'ai enregistré dans le home de l'utilisateur jitsi.

```sh
cd /opt/jitsi
sudo -u jitsi git clone https://github.com/jitsi/jitsi-meet.git
cd jitsi-meet
sudo -u jitsi npm install
sudo -u jitsi make -j1
```

Il est nécessaire de modifier les permissions du dossier `/opt/jitsi/jitsi-meet` afin d'en autoriser sa lecture à l'utilisateur **http** (puisque c'est lui qui exécutera le serveur http).

```sh
chown jitsi:http /opt/jitsi
chmod g+x /opt/jitsi

chown -R jitsi:http /opt/jitsi/jitsi-meet
chmod -R g+r /opt/jitsi/jitsi-meet
```

Puis, modifiez le fichier `/opt/jitsi/jitsi-meet/config.js` de manière à y faire figurer votre nom de domaine.

## Configuration de Nginx

Pour commencer, on créer le fichier `/etc/nginx/sites-available/jitsi.conf`.

```
server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;

        server_name jitsi.<DOMAINE>;
        include includes/jitsi.<DOMAINE>-https.conf;
        
        # set the root
        root /opt/jitsi/jitsi-meet;
        index index.html;

        location ~ ^/([a-zA-Z0-9=\?]+)$ {
                rewrite ^/(.*)$ / break;
        }

        location / {
                ssi on;
        }

        # BOSH, Bidirectional-streams Over Synchronous HTTP
        # https://en.wikipedia.org/wiki/BOSH_(protocol)
        location /http-bind {
                proxy_pass      http://localhost:5280/http-bind;
                proxy_set_header X-Forwarded-For $remote_addr;
                proxy_set_header Host $http_host;
        }

        # external_api.js must be accessible from the root of the
        # installation for the electron version of Jitsi Meet to work
        # https://github.com/jitsi/jitsi-meet-electron
        location /external_api.js {
                alias /opt/jitsi/jitsi-meet/libs/external_api.min.js;
        }
}
```

Ensuite, il faut créer le fichier `/etc/nginx/includes/jitsi.<DOMAINE>-https.conf`. Pour ce faire, je m'aide de [cet outil](https://ssl-config.mozilla.org/), proposé par Mozilla. Il suffit simplement de copier-coller ce que le site nous donne !

Ensuite, il faut informer nginx qu'il doit servir l'url `jitsi.<DOMAINE>` en créant un lien symbolique :

```
ln -s /etc/nginx/sites-available/jitsi.conf /etc/nginx/sites-enabled/jitsi.conf
```

Pensez également à vérifier que le fichier `/etc/nginx/nginx.conf` lit bel et bien les fichiers présents dans `/etc/nginx/sites-enabled`. La ligne `include sites-enabled/*;` doit se trouver dans le bloc `http`.

Enfin, on peut démarrer Nginx, ou le recharger s'il est déjà lancé.

```sh
systemctl start nginx
# Ou
systemctl reload nginx
```

Normalement, Jitsi-Meet est accessible depuis l'URL `https://jitsi.<DOMAINE>` ! Il reste d'autres choses à faire, comme par exemple rediriger toutes le connexions sur le port 80 (HTTP , donc non chiffrée) vers le port 443 (HTTPS, donc chiffrée). Il peut également être intéressant d'héberger son propre serveur STUN/TURN plutôt que celui utilisé par défaut.