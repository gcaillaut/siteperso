+++
title = "My Blog"
template = "blog.html"
permalink = "/blog/"
sort_by = "date"
paginate_by = 10
insert_anchor_links = "left"

[extra]
hide_title = true
+++
