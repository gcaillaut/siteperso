+++
title="Install Jitsi-Meet on an Archlinux server"
draft=false

[extra]

[taxonomies]
tags=["jitsi-meet"]
+++

Last time, I wanted to host an instance of [Jitsi-Meet](https://github.com/jitsi/jitsi-meet), an open-source video conferencing software, on my server running under [Archlinux](https://www.archlinux.org/). Unfortunately, the official Jitsi-Meet documentation only covers installation on a distribution derived from Debian and I have not managed to have a functional installation from packages available on the AUR.

This guide, based on the [official documentation](https://github.com/jitsi/jitsi-meet/blob/master/doc/manual-install.md), aims to describe each step leading to the installation from Jitsi-Meet on Archlinux, but it should be suitable for any other distribution.

<!-- more -->

Jitsi-Meet is *only* the interface around the Jitsi software. It is thus necessary to install several softwares in order to have a functional installation, namely:

- [Jitsi-Meet](https://github.com/jitsi/jitsi-meet), obviously
- [Jitsi-Videobridge](https://github.com/jitsi/jitsi-videobridge)
- [JItsi COnference FOcus (Jicofo)](https://github.com/jitsi/jicofo)
- An XMPP server, I use [Prosody](https://prosody.im/) but it may be possible to use another one
- An HTTP server, I use [Nginx](https://nginx.org/)

Each of these softwares communicate as indicated below. The numbers indicate the ports used.

```
                   +                           +
                   |                           |
                   |                           |
                   v                           |
                  443                          |
               +-------+                       |
               |       |                       |
               | Nginx |                       |
               |       |                       |
               +--+-+--+                       |
                  | |                          |
+------------+    | |    +--------------+      |
|            |    | |    |              |      |
| jitsi-meet +<---+ +--->+ prosody/xmpp |      |
|            |files 5280 |              |      |
+------------+           +--------------+      v
                     5222,5347^    ^5347   4443,10000
                +--------+    |    |    +-------------+
                |        |    |    |    |             |
                | jicofo +----^    ^----+ videobridge |
                |        |              |             |
                +--------+              +-------------+
```

Let's start by installing these softwares.

## Prosody

## Installation

Prosody is available on archlinux's repositories. Beware of the version of lua used: the lua-sec library seems to malfunction with Prosody on any version versions of Lua other than 5.2.

```sh
sudo pacman -S prosody lua52-sec cyrus-sasl
```

If it was not done during the installation, update the right of the `/var/lib/prosody` folder.

```sh
sudo chown -R prosody:prosody /var/lib/prosody
```

## Configuration

The content of my file `/etc/prosody/prosody.cfg.lua` is given below.

```lua
--Important for systemd
-- daemonize is important for systemd. if you set this to false the systemd startup will freeze.
daemonize = true
pidfile = "/run/prosody/prosody.pid"

-- Prosody Example Configuration File
--
-- Information on configuring Prosody can be found on our
-- website at https://prosody.im/doc/configure
--
-- Tip: You can check that the syntax of this file is correct
-- when you have finished by running this command:
--     prosodyctl check config
-- If there are any errors, it will let you know what and where
-- they are, otherwise it will keep quiet.
--
-- The only thing left to do is rename this file to remove the .dist ending, and fill in the
-- blanks. Good luck, and happy Jabbering!


---------- Server-wide settings ----------
-- Settings in this section apply to the whole server and are the default settings
-- for any virtual hosts

-- This is a (by default, empty) list of accounts that are admins
-- for the server. Note that you must create the accounts separately
-- (see https://prosody.im/doc/creating_accounts for info)
-- Example: admins = { "user1@example.com", "user2@example.net" }
admins = { }

cross_domain_bosh = true;
component_ports = { 5347 }
-- component_interface = "127.0.0.1"

-- Enable use of libevent for better performance under high load
-- For more information see: https://prosody.im/doc/libevent
--use_libevent = true

-- Prosody will always look in its source directory for modules, but
-- this option allows you to specify additional locations where Prosody
-- will look for modules first. For community modules, see https://modules.prosody.im/
--plugin_paths = {}

-- This is the list of modules Prosody will load on startup.
-- It looks for mod_modulename.lua in the plugins folder, so make sure that exists too.
-- Documentation for bundled modules can be found at: https://prosody.im/doc/modules
modules_enabled = {

        -- Generally required
                "roster"; -- Allow users to have a roster. Recommended ;)
                "saslauth"; -- Authentication for clients and servers. Recommended if you want to log in.
                "tls"; -- Add support for secure TLS on c2s/s2s connections
                "dialback"; -- s2s dialback support
                "disco"; -- Service discovery
                "posix"; -- POSIX functionality, sends server to background, enables syslog, etc.

        -- Not essential, but recommended
                "carbons"; -- Keep multiple clients in sync
                "pep"; -- Enables users to publish their avatar, mood, activity, playing music and more
                "private"; -- Private XML storage (for room bookmarks, etc.)
                "blocklist"; -- Allow users to block communications with other users
                "vcard4"; -- User profiles (stored in PEP)
                "vcard_legacy"; -- Conversion between legacy vCard and PEP Avatar, vcard

        -- Nice to have
                "version"; -- Replies to server version requests
                "uptime"; -- Report how long server has been running
                "time"; -- Let others know the time here on this server
                "ping"; -- Replies to XMPP pings with pongs
                "register"; -- Allow users to register on this server using a client and change passwords
                --"mam"; -- Store messages in an archive and allow users to access it
                --"csi_simple"; -- Simple Mobile optimizations

        -- Admin interfaces
                "admin_adhoc"; -- Allows administration via an XMPP client that supports ad-hoc commands
                --"admin_telnet"; -- Opens telnet console interface on localhost port 5582

        -- HTTP modules
                "bosh"; -- Enable BOSH clients, aka "Jabber over HTTP"
                --"websocket"; -- XMPP over WebSockets
                --"http_files"; -- Serve static files from a directory over HTTP

        -- Other specific functionality
                --"limits"; -- Enable bandwidth limiting for XMPP connections
                --"groups"; -- Shared roster support
                --"server_contact_info"; -- Publish contact information for this service
                --"announce"; -- Send announcement to all online users
                --"welcome"; -- Welcome users who register accounts
                --"watchregistrations"; -- Alert admins of registrations
                --"motd"; -- Send a message to users when they log in
                --"legacyauth"; -- Legacy authentication. Only used by some old clients and bots.
                --"proxy65"; -- Enables a file transfer proxy service which clients behind NAT can use

        -- jitsi
                "smacks";
                "carbons";
                "mam";
                "lastactivity";
                "offline";
                "pubsub";
                "adhoc";
                "websocket";
                "http_altconnect";
}

-- These modules are auto-loaded, but should you want
-- to disable them then uncomment them here:
modules_disabled = {
        -- "offline"; -- Store offline messages
        -- "c2s"; -- Handle client connections
        -- "s2s"; -- Handle server-to-server connections
        -- "posix"; -- POSIX functionality, sends server to background, enables syslog, etc.
}

-- Disable account creation by default, for security
-- For more information see https://prosody.im/doc/creating_accounts
allow_registration = false

-- Force clients to use encrypted connections? This option will
-- prevent clients from authenticating unless they are using encryption.

-- c2s_require_encryption = true

-- Force servers to use encrypted connections? This option will
-- prevent servers from authenticating unless they are using encryption.

-- s2s_require_encryption = true

-- Force certificate authentication for server-to-server connections?

-- s2s_secure_auth = true

-- Some servers have invalid or self-signed certificates. You can list
-- remote domains here that will not be required to authenticate using
-- certificates. They will be authenticated using DNS instead, even
-- when s2s_secure_auth is enabled.

--s2s_insecure_domains = { "insecure.example" }

-- Even if you disable s2s_secure_auth, you can still require valid
-- certificates for some domains by specifying a list here.

--s2s_secure_domains = { "jabber.org" }

-- Select the authentication backend to use. The 'internal' providers
-- use Prosody's configured data storage to store the authentication data.

authentication = "internal_hashed"

-- Select the storage backend to use. By default Prosody uses flat files
-- in its configured data directory, but it also supports more backends
-- through modules. An "sql" backend is included by default, but requires
-- additional dependencies. See https://prosody.im/doc/storage for more info.

--storage = "sql" -- Default is "internal"

-- For the "sql" backend, you can uncomment *one* of the below to configure:
--sql = { driver = "SQLite3", database = "prosody.sqlite" } -- Default. 'database' is the filename.
--sql = { driver = "MySQL", database = "prosody", username = "prosody", password = "secret", host = "localhost" }
--sql = { driver = "PostgreSQL", database = "prosody", username = "prosody", password = "secret", host = "localhost" }


-- Archiving configuration
-- If mod_mam is enabled, Prosody will store a copy of every message. This
-- is used to synchronize conversations between multiple clients, even if
-- they are offline. This setting controls how long Prosody will keep
-- messages in the archive before removing them.

archive_expires_after = "1w" -- Remove archived messages after 1 week

-- You can also configure messages to be stored in-memory only. For more
-- archiving options, see https://prosody.im/doc/modules/mod_mam

-- Logging configuration
-- For advanced logging see https://prosody.im/doc/logging
log = {
        -- info = "prosody.log"; -- Change 'info' to 'debug' for verbose logging
        -- error = "prosody.err";
        "*syslog"; -- Uncomment this for logging to syslog
        -- "*console"; -- Log to the console, useful for debugging with daemonize=false
}

-- Uncomment to enable statistics
-- For more info see https://prosody.im/doc/statistics
-- statistics = "internal"

-- Certificates
-- Every virtual host and component needs a certificate so that clients and
-- servers can securely verify its identity. Prosody will automatically load
-- certificates/keys from the directory specified here.
-- For more information, including how to use 'prosodyctl' to auto-import certificates
-- (from e.g. Let's Encrypt) see https://prosody.im/doc/certificates

-- Location of directory to find certificates in (relative to main config file):
certificates = "certs"

-- HTTPS currently only supports a single certificate, specify it here:
--https_certificate = "/etc/prosody/certs/localhost.crt"

----------- Virtual hosts -----------
-- You need to add a VirtualHost entry for each domain you wish Prosody to serve.
-- Settings under each VirtualHost entry apply *only* to that host.

VirtualHost "jitsi.<DOMAIN>"
    authentication = "anonymous"
    modules_enabled = {
        "bosh";
        "pubsub";
    }
    c2s_require_encryption = false

VirtualHost "auth.jitsi.<DOMAIN>"
    authentication = "internal_plain"

admins = { "focus@auth.jitsi.<DOMAIN>" }

------ Components ------
-- You can specify components to add hosts that provide special services,
-- like multi-user conferences, and transports.
-- For more information on components, see https://prosody.im/doc/components

---Set up a MUC (multi-user chat) room server on conference.example.com:
--Component "conference.example.com" "muc"
--- Store MUC messages in an archive and allow users to access it
--modules_enabled = { "muc_mam" }

---Set up an external component (default component port is 5347)
--
-- External components allow adding various services, such as gateways/
-- transports to other networks like ICQ, MSN and Yahoo. For more info
-- see: https://prosody.im/doc/components#adding_an_external_component
--
--Component "gateway.example.com"
--      component_secret = "password"

Component "conference.jitsi.<DOMAIN>" "muc"

Component "jitsi-videobridge.jitsi.<DOMAIN>"
    component_secret = "<SECRET1>"

Component "focus.jitsi.<DOMAIN>"
    component_secret = "<SECRET2>"
```

It is necessary to replace the `<DOMAIN>`, `<SECRET1>` and `<SECRET2>` by, respectively, a domain name and two *secrets* which will be used to authenticate the various agents of the system. In this way, only the videobridge can communicate with the `jitsi-videobridge.jitsi.<DOMAIN>` component.

Finally, add a user, named **focus** and set its password to `<SECRET3>`.

```sh
prosodyctl register focus auth.jitsi.<DOMAIN> <SECRET3>
```

I used *pwgen* to generate my three secrets, all made up of 30 characters. It is available from the Archlinux repositories.

```sh
pwgen 30 3
```

It's also possible to use openssl:

```sh
openssl rand -base64 30
```

## SSL certificates

Then, I generated an SSL certificates for each URL mentioned in the `/etc/prosody/prosody.cfg.lua` file. I used [certbot] (https://certbot.eff.org/), which is a softaware that allows anyone to easily retrieve certificates signed by the [Let's Encrypt](https://letsencrypt.org/) authority.

```
sudo pacman -S certbot
```

The [certbot](https://certbot.eff.org/) website details each step to obtain a certificate. As far as I'm concerned, I entered the following commands, as **root** (in my case, I also had to install the `certbot-dns-ovh` package):

```sh
certbot certonly --dns-ovh --dns-ovh-credentials ~/.config/.ovh-credentials.txt -d jitsi.<DOMAIN>
certbot certonly --dns-ovh --dns-ovh-credentials ~/.config/.ovh-credentials.txt -d auth.jitsi.<DOMAIN>
```

But, in most cases, the following commands should suffice (the `-d` option allows you to indicate the domain name to be certified):

```sh
sudo certbot certonly --nginx -d jitsi.<DOMAIN>
sudo certbot certonly --nginx -d auth.jitsi.<DOMAIN>
```

The certificates are then installed in `/etc/letsencrypt/live/jitsi.<DOMAIN>` and `/etc/letsencrypt/live/auth.jitsi.<DOMAIN>`. However, these folders are only accessible by the **root** user, not by **prosody**. It is then necessary to import the certificates using the following command (as **root**):

```sh
prosodyctl --root cert import /etc/letsencrypt/live
```

Prosody's [documentation](https://prosody.im/doc/letsencrypt) indicates how to execute this command with each renewal of certificates:

```
certbot renew --deploy-hook "prosodyctl --root cert import /etc/letsencrypt/live"
```

We can now start Prosody.


```sh
sudo systemctl start prosody.service
```

# Jitsi videobridge

## Installation

To start, I created the user * jitsi *. It is this user who will execute the various programs.

```
useradd jitsi -m -d /opt/jitsi -s /usr/bin/nologin
```

I chose to rely on the binaries precompiled by the team behind Jitsi. You have to download them and then decompress them.

```sh
cd /opt/jitsi
wget https://download.jitsi.org/jitsi-videobridge/linux/jitsi-videobridge-linux-x64-1132.zip
unzip jitsi-videobridge-linux-x64-1132.zip
rm -r jitsi-videobridge-linux-x64-1132.zip
chown -R jitsi:jitsi jitsi-videobridge-linux-x64-1132
```

Then I created the file `/opt/jitsi/.sip-communicator/sip-communicator.properties`. This very important file allows the videobridge to communicate the address of the WebRTC server to each of the participants. More details on the different modifiable parameters are given [here](https://github.com/jitsi/ice4j/blob/master/doc/configuration.md).

```sh
org.jitsi.impl.neomedia.transform.srtp.SRTPCryptoContext.checkReplay=false

# The videobridge uses 443 by default with 4443 as a fallback, but since we're already
# running nginx on 443 in this example doc, we specify 4443 manually to avoid a race condition
org.jitsi.videobridge.TCP_HARVESTER_PORT=4443
org.jitsi.videobridge.TCP_HARVESTER_MAPPED_PORT=4443

org.ice4j.ice.harvest.NAT_HARVESTER_LOCAL_ADDRESS=<IP LOCALE>
# L'adresse IP globale peut être récupérer, par exemple, depuis https://duckduckgo.com/?q=my+ip
org.ice4j.ice.harvest.NAT_HARVESTER_PUBLIC_ADDRESS=<IP GLOBALE>

org.ice4j.ice.harvest.DISABLE_AWS_HARVESTER=true
```

Then we create the file */etc/jitsi-videobridge/jitsi-videobridge.conf*. It is necessary to replace `<DOMAIN>` and `<SECRET1>` by the same values as in the configuration file of Prosody.

```
flags="--host=localhost --domain=jitsi.<DOMAIN> --port=5347 --secret=<SECRET1>"
```

Finally, we create the file *jitsi-videobridge.service* which we copy into `/etc/systemd/system`.

```
[Unit]
Description=Jitsi Videobridge
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
User=jitsi
Group=jitsi
EnvironmentFile=/etc/jitsi-videobridge/jitsi-videobridge.conf
ExecStart=/opt/jitsi/jitsi-videobridge-linux-x64-1132/jvb.sh ${flags}
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

It is necessary to reload systemd.

```sh
sudo systemctl daemon-reload
sudo systemctl start jitsi-videobridge.service
```

# Jicofo

## Installation

This time, I decided to compile Jicofo from its sources.

```sh
cd /opt/jitsi
git clone https://github.com/jitsi/jicofo.git
chown -R jitsi:jitsi jicofo
cd jicofo
sudo -u jitsi mvn package -DskipTests -Dassembly.skipAssembly=false
sudo -u jitsi unzip target/jicofo-1.1-SNAPSHOT-archive.zip
```

## Configuration

As with the videobridge, we first create the configuration file `/etc/jicofo/jicofo.conf`.

```
flags="--host=localhost --domain=jitsi.<DOMAIN> --secret=<SECRET2> --user_domain=auth.jitsi.<DOMAIN> --user_name=focus --user_password=<SECRET3>"
```

Then we create the file *jicofo.service*, which I copied to `/etc/systemd/system`.

```
[Unit]
Description=Jicofo
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
User=jitsi
Group=jitsi
EnvironmentFile=/etc/jicofo/jicofo.conf
ExecStart=/opt/jitsi/jicofo/jicofo-1.1-SNAPSHOT/jicofo.sh ${flags}
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

We can then start the jicofo service.

```sh
sudo systemctl daemon-reload
sudo systemctl start jicofo.service
```

# Jitsi Meet

The last step is to install the web application: Jitsi-Meet. At first, it is necessary to install the nginx package. The user **http** should be created during installation.

```
pacman -S nginx-mainline
```

## Downloading the sources

I got the sources of Jitsi-Meet, which I saved in the home of the user jitsi.

```sh
cd /opt/jitsi
sudo -u jitsi git clone https://github.com/jitsi/jitsi-meet.git
cd jitsi-meet
sudo -u jitsi npm install
sudo -u jitsi make -j1
```

It is necessary to modify the permissions of the folder `/opt/jitsi/jitsi-meet` in order to authorize its reading to the user **http** (since it is he who will run the http server).

```sh
chown jitsi:http /opt/jitsi
chmod g+x /opt/jitsi

chown -R jitsi:http /opt/jitsi/jitsi-meet
chmod -R g+r /opt/jitsi/jitsi-meet
```

Then, modify the file `/opt/jitsi/jitsi-meet/config.js` so as to include your domain name.

## Nginx configuration

To start, we create the file `/etc/nginx/sites-available/jitsi.conf`.

```
server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;

        server_name jitsi.<DOMAIN>;
        include includes/jitsi.<DOMAIN>-https.conf;
        
        # set the root
        root /opt/jitsi/jitsi-meet;
        index index.html;

        location ~ ^/([a-zA-Z0-9=\?]+)$ {
                rewrite ^/(.*)$ / break;
        }

        location / {
                ssi on;
        }

        # BOSH, Bidirectional-streams Over Synchronous HTTP
        # https://en.wikipedia.org/wiki/BOSH_(protocol)
        location /http-bind {
                proxy_pass      http://localhost:5280/http-bind;
                proxy_set_header X-Forwarded-For $remote_addr;
                proxy_set_header Host $http_host;
        }

        # external_api.js must be accessible from the root of the
        # installation for the electron version of Jitsi Meet to work
        # https://github.com/jitsi/jitsi-meet-electron
        location /external_api.js {
                alias /opt/jitsi/jitsi-meet/libs/external_api.min.js;
        }
}
```

Next, create the file `/etc/nginx/includes/jitsi.<DOMAIN>-https.conf`. To do this, I use [this tool](https://ssl-config.mozilla.org/), from Mozilla. You just have to copy and paste what the site gives us!

Then, you must inform nginx that it must serve the url `jitsi.<DOMAIN>` by creating a symbolic link:

```
ln -s /etc/nginx/sites-available/jitsi.conf /etc/nginx/sites-enabled/jitsi.conf
```

Also remember to check that the `/etc/nginx/nginx.conf` file does indeed read the files found in `/etc/nginx/sites-enabled`. The line `include sites-enabled / *;` must be in the block `http`.

Finally, you can start Nginx, or reload it if it is already launched.

```sh
systemctl start nginx
# Or
systemctl reload nginx
```

Normally, Jitsi-Meet is accessible from the URL `https://jitsi.<DOMAIN>`! There are other things to do, such as redirecting all connections on port 80 (HTTP, therefore unencrypted) to port 443 (HTTPS, therefore encrypted). It can also be interesting to host your own STUN / TURN server rather than the one used by default.