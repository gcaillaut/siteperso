+++
title = "Home"
+++

Welcome to my website. I am Gaëtan Caillaut and I am a temporary lecturer and research assistant of the [university of Orléans](https://www.univ-orleans.fr/en).

I am doing my teaching duty in the [computer science department](http://www.univ-orleans.fr/en/iut-orleans/informatique) of the University Institute of Technology of Orléans and my research is done at the [laboratory of computer science](https://www.univ-orleans.fr/lifo?lang=en) of Orléans.

My thesis, which was supervised by [Guillaume Cleuziou](https://sites.google.com/site/guillaumecleuzioulifo/), is titled <q>Learning pretopological spaces for structured knowledge acquissition</q>.
