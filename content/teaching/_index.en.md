+++
title = "Teaching"

[extra]
hide_title = false
+++

<!-- toc -->

## Summary of my teaching activities

| Year        | Name                                              | Level   | Tutorials | PW    | Total éq. TD |
| ------------|---------------------------------------------------|---------|-----------|-------|--------------|
| 2019 - 2020 | Basics of object-oriented design                  | 1A      | 0         | 12    | 8            |
|             | Introduction to HCI                               | 1A      | 12        | 0     | 12           |
|             | Introduction to databases                         | 1A      | 0         | 18    | 12           |
|             | Application production methodology                | AS      | 30        | 0     | 30           |
|             | Internship supervision                            | AS      | 6         | 0     | 6            |
|             | Operating system's principles                     | 2A      | 10.5      | 14    | 20           |
|             | Operational research and decision support         | 2A      | 14        | 52.5  | 49           |
|             | Internship supervision                            | 2A      | 4         | 0     | 4            |
|             | Web development with Django                       | LP      | 31.5      | 0     | 31.5         |
| 2018 - 2019 | Operational research and decision support         | 2A      | 7         | 21    | 21           |
|             | Distributed programming                           | 2A      | 7         | 21    | 21           |
|             | Internship supervision                            | 2A      | 2         | 0     | 2            |
|             | Web development with Django                       | LP      | 6.5       | 0     | 6,5          |
| 2017 - 2018 | Operational research and decision support         | 2A      | 7         | 21    | 21           |
|             | Web development with Django                       | LP      | 34        | 0     | 34           |
|             | Internship supervision                            | LP      | 2         | 0     | 2            |
| 2016 - 2017 | Data structures and fundamental algorithms        | 1A      | 16        | 16    | 26.5         |
|             | Operational research and decision support         | 2A      | 8         | 24    | 24           |
|             |                                                   | Totals: | 197.5     | 199.5 | 330.5        |

## DUT first year

### Basics of object-oriented design

The goal of this course is to teach the basics of object-oriented design to first year students. To this aim, the tools offered by the UML language are presented. Students discovers how to write several digrams, such as use case diagrams, class diagrams, objec diagrams of sequence diagrams.

This course also introduces some notions of object-oriented programming, such as interfaces, inheritance and data encapsulation.

### Introduction to HCI

In this course, students learn how to design and develop the GUI of an application, as well as some principles of Human–computer interaction (HCI).

The aim of the tutorial session is to define the application needs with the help of UML diagrams. Then, students design mockups.

Practical work sessions are dedicated to the development of GUI with the JavaFX library.

### Introduction to databases

Students learn to design, administer and query databases. I only supervised the practical work sessions, in which students write queries, firstly in relational algebra, then in SQL, to interrogate an Oracle database.

Queries seen in practical work session cover the *classical needs*: selection/update/deletion queries, table creation/deletion and joins.

### Data structures and fundamental algorithms

This course is one of the first taught to first year students. As a consequence, students generally do not have any programming knowledge, this is why this course require an important didactic effort.

Through the Python language, students learn to write simple programe and to use data structures available in the Python standard library (mostly lists, dictionnaries, and sets). Simple algorithms, such as sort algorithms, are also introduced throughout the tutorials sessions.

These tutorials also aims to teach students how object are stored in a computer memory, which bascally consists in a first introduction to pointers. Students are also made aware of the concept of algorithmic complexity.

## DUT special year

The *special* year is a on year formation, instead of two, delivering the DUT diploma.

### Application production methodology

This module teaches students the different steps leading to the realization of an application. The module is split into two parts: the first, more theoretical, concerns the design phase of the application and the second, more applied, concerns the development phase of the application.

During the first part, students are responsible for designing a supply management application.
The design of the information system is carried out using the tools offered by the Merise method (MCD and MLD). Students also learn to write queries in relational algebra.
The functionality of the application is described using the tools offered by UML, in particular through use case diagrams. Finally, the students offer a first draft of the interface of their application using models written on paper or made using drawing software.

During the second part of the module, students use the Access software to develop the application conceptualized during the first part of the module. Access software acts as an *all-in-one* tool by providing an interface in which students can design their database, create queries and create the user interface for their application. The realization of the interface allows to introduce the concepts of event programming, which is  very different from the concepts of imperative programming to which students are accustomed.

## DUT second year

### Operating system's principles

The objective of this module is to teach students various notions of system programming: execution threads, representation in memory of a program, pointers, inputs/outputs and compilation.

First, the students are introduced to the C language and to certain compilation tools (gcc and make). This initiation is an opportunity to introduce them to the various stages necessary for the production of an executable binary program (preprocessor, assembly and linking).

Students then learn to retrieve information from a process (typically, its PID) in order to observe the memory organization of the program it executes. The goal is to show that different areas of memory exist to store constant variables, allocated statically in the stack or dynamically on the heap. Other notions of compilation are seen, such as the compilation of shared and static libraries and the use of these.

Finally, students learn to write parallel programs using the pthread library.

### Distributed programming

The objective of this module is to introduce the concepts of parallel programming and some of its constraints: scheduling, deadlock, concurrent access ..

Students are led to solve various classic problems (Readers / Writers, Producers / Consumers) using parallel programs written in the Java language. Java indeed offers a certain number of very practical tools for developing such programs: monitor, mutex, semaphores ...

### Operational research and decision support

This module is an introduction to the problems of classification, machine learning and data mining. As such, some *simple* algorithms (APriori, k closest neighbors and naive Bayesian classifier) ​​are presented to students during tutorials. These algorithms are then implemented in the R language during practical work sessions.

Students are then introduced to text mining and document classification. In particular, students learn how to import and clean a corpora, as well as how to extract relevant descriptors from it. Students are finally rated through a *challenge* in which they must develop a pipeline in order to clean and classify a set of documents. To this end, I produced an automatic evaluation website allowing students to quickly get feedbacks about the performance of their classifier.


## Professional bachelor (third year)

### Web development with Django

The students of the professional license course come mainly from the IUT of Orléans, where Python is taught. Some, however, come from other establishments and do not necessarily know the Python language. The first tutorials and practical work sessions of this module consist of a quick reminder of Python programming for the majority of students and an accelerated training for the others.

The [Django](https://www.djangoproject.com/) library being quite substantial, it is not possible to present everything to the students, however a fairly large portion of what is doable is covered: creation of view with the MVT design pattern, conception of database models with Django's ORM, updating the database schema with migration tools, unit tests. These concepts, although taught through Django, are fairly general and exist in other web libraries.

Other best practices are also presented, such as using virtual environments (virtualenv) to partition a project or source code versionning with git.