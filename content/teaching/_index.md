+++
title = "Enseignements"

[extra]
hide_title = false
+++

<!-- toc -->

## Synthèse de mes activités d'enseignement

Tous mes enseignements ont été effectués à l'IUT informatique d'Orléans.

| Année       | Intitulé                                          | Niveau   | TD    | TP    | Total éq. TD |
| ------------|---------------------------------------------------|----------|-------|-------|--------------|
| 2019 - 2020 | Bases de la conception orientée objet             | 1A       | 0     | 12    | 8            |
|             | Introduction aux IHM                              | 1A       | 12    | 0     | 12           |
|             | Introduction aux bases de données                 | 1A       | 0     | 18    | 12           |
|             | Méthodologie de la production d'applications      | AS       | 30    | 0     | 30           |
|             | Encadrement de stages                             | AS       | 6     | 0     | 6            |
|             | Principes des systèmes d'exploitation             | 2A       | 10,5  | 14    | 20           |
|             | Recherche opérationnelle et aide à la décision    | 2A       | 14    | 52,5  | 49           |
|             | Encadrement de stages                             | 2A       | 4     | 0     | 4            |
|             | Développement web avec Django                     | LP       | 31,5  | 0     | 31,5         |
| 2018 - 2019 | Recherche opérationnelle et aide à la décision    | 2A       | 7     | 21    | 21           |
|             | Programmation répartie                            | 2A       | 7     | 21    | 21           |
|             | Encadrement de stages                             | 2A       | 2     | 0     | 2            |
|             | Développement web avec Django                     | LP       | 6,5   | 0     | 6,5          |
| 2017 - 2018 | Recherche opérationnelle et aide à la décision    | 2A       | 7     | 21    | 21           |
|             | Développement web avec Django                     | LP       | 34    | 0     | 34           |
|             | Encadrement de stages                             | LP       | 2     | 0     | 2            |
| 2016 - 2017 | Structures de données et algorithmes fondamentaux | 1A       | 16    | 16    | 26,5         |
|             | Recherche opérationnelle et aide à la décision    | 2A       | 8     | 24    | 24           |
|             |                                                   | Totaux : | 197,5 | 199,5 | 330,5        |

## DUT première année

### Bases de la conception orientée objet

L'objectif de ce module est d'enseigner aux étudiants de première année comment concevoir une application, à l'aide des outils de modélisation proposés par UML. Les étudiants découvrent et apprennent à produire différents diagrammes, tels que les diagrammes de cas d'utilisation, de classes, d'objets ou de séquences.

Ce module permet également d'introduire certaines notions de programmation orientée objet, telles que les interfaces, l'héritage ou l'encapsulation de données avec les visibilités publiques, protégées et privées.

### Introduction aux IHM

Dans ce module, les étudiants apprennent à concevoir et réaliser l'interface utilisateur d'une application.

Les séances de TD permettent de définir les besoins de l'application à l'aide de diagramme de cas d'utilisation ou de séquences proposé par le langage UML. Une fois les besoins de l'application déterminés, les étudiants réalisent plusieurs maquettes répondant à ces besoins.

Les séances de TP, que je n'ai pas encadré, consistent à réaliser des interfaces utilisateur à l'aide de la bibliothèque JavaFX.

### Introduction aux bases de données

Dans ce module, les étudiants apprennent à concevoir, administrer et interroger des bases de données. J'ai uniquement encadré les TPs, durant lesquels les étudiants écrivent, dans un premier temps, des requêtes en algèbre relationnelle puis, dans un second temps, des requêtes SQL à destination d'une base de données Oracle.

Les requêtes vues au cours de ces TP couvrent la plupart des besoins *classiques* : requêtes de sélection, de mise à jour et de suppression, requêtes de création de tables et jointures naturelles ou non.

### Structures de données et algorithmes fondamentaux

Ce module à lieu au début du premier semestre et s'adresse donc à des étudiants n'ayant, pour la plupart, aucune connaissance en programmation. De ce fait, il requiert un effort didactique important.

Au travers du langage Python, les étudiants apprennent à écrire des programmes simples et à utiliser les structures de données disponibles dans le langage Python (listes, dictionnaires et ensembles). Des algorithmes simples et classiques, tels que les tris, sont également introduits dans ce module.

Les TD de ce module sont principalement destinés à faire comprendre aux étudiants la manière dont sont représentés les objets en mémoire, ce qui constitue une première introduction à la notion de pointeur. D'autre part, les étudiants sont également sensibilisés au concept de complexité algorithmique.

## DUT année spéciale

Le DUT année spéciale est une formation d'un an délivrant le diplôme de DUT.
Les étudiants en année spéciale doivent justifier d'un bac +2 (ou équivalent).

### Méthodologie de la production d'applications

Ce module permet d'enseigner aux étudiants les différentes étapes menant à la réalisation d'une application. Le module est scindé en deux parties : la première, plus théorique, concerne la phase de conception de l'application et la seconde, plus appliquée, concerne la phase de développement de l'application.

Lors de la première partie, les étudiants sont chargés de concevoir une application de gestion de fournitures.
La conception du système d'informations est réalisée à l'aide des outils proposés par la méthode Merise (MCD et MLD). Les étudiants apprennent également à écrire des requêtes en algèbre relationnelle.
Les fonctionnalités de l'application sont quant à elles décrites à l'aide des outils proposés par UML, notamment aux travers de diagrammes de cas d'utilisation. Enfin, les étudiants proposent une première ébauche de l'interface de leur application à l'aide de maquettes réalisées sur papier ou bien à l'aide d'un logiciel de dessin.

Lors de la seconde partie du module, les étudiants utilisent le logiciel Access afin de développer l'application conceptualisée lors de la première partie du module. Le logiciel Access fait office d'outil *tout-en-un* en proposant une interface dans laquelle les étudiants peuvent concevoir leur base de données, créer des requêtes et réaliser l'interface utilisateur de leur application. La réalisation de l'interface permet d'introduire les concepts de la programmation évènementielle, bien différents des concepts de la programmation impérative auxquelles les étudiants sont habitués.

## DUT deuxième année

### Principes des systèmes d'exploitation

L'objectif de ce module est d'enseigner aux étudiants diverses notions de programmation système : fils d'exécution, représentation en mémoire d'un programme, pointeurs, entrées/sorties et compilation.

Dans un premier temps les étudiants sont initiés au langage C et à certains outils de compilation (gcc et make). Cette initiation est l'occasion de leur présenter les diverses étapes nécessaires à la production d'un programme binaire exécutable (préprocesseur, assemblage et édition de liens).

Les étudiants apprennent ensuite à récupérer les informations d'un processus (typiquement, son PID) dans le but d'observer l'organisation mémoire du programme qu'il exécute. L'objectif étant de montrer que différentes zones de la mémoire existent pour stocker des variables constantes, allouées statiquement dans la pile ou dynamiquement sur le tas. D'autres notions de compilation sont vues, comme la compilation de bibliothèques partagées et statiques ainsi que l'utilisation de celles-ci.

Enfin, les étudiants apprennent à écrire des programmes parallèles à l'aide de la bibliothèque pthread.

### Programmation répartie

L'objectif de ce module est d'introduire les concepts de programmation parallèle, et surtout les problèmes qui en découlent : ordonnancement, interblocage, accès concurrents...

Les étudiants sont amenés à résoudre différents problèmes classiques (Lecteurs/Rédacteurs, Producteurs/Consommateurs) à l'aide de programme parallèles écrits dans le langage Java. Java propose en effet un certain nombre d'outils très pratiques pour développer de tels programmes : moniteur, mutex, sémaphores...

### Recherche opérationnelle et aide à la décision

Ce module constitue une introduction aux problèmes de classification, d'apprentissage automatique et de fouille de données. À ce titre, certains algorithmes *simples* (APriori, k plus proches voisins et classifieur Bayésien naïf) sont présentés aux étudiants à l'occasion des séances de travaux dirigés. Ces mêmes algorithmes sont par la suite implantés dans le langage R lors des séances de travaux pratiques.

Les étudiants sont, par la suite, sensibilisés aux problèmes de fouille et de classification de textes. Notamment, les étudiants apprennent comment importer un corpus, comment le nettoyer et comment en extraire des descripteurs pertinents. Les étudiants sont finalement notés au travers d'un *challenge* dans lequel ils doivent, par groupe, proposer une chaine de traitements afin de nettoyer et classer des documents. J'ai, à cet effet, produit un site d'évaluation automatique permettant aux étudiants d'obtenir rapidement un retour sur les performances de leur programme.


## Licence professionnelle troisième année

### Développement web avec Django

Les étudiants du parcours licence professionnelle proviennent majoritairement de l'IUT d'Orléans. Certains proviennent toutefois d'autres établissement et ne connaissent pas nécessairement le langage Python. Les premiers TDs et TPs de ce module consistent alors en un rapide rappel de la programmation avec Python pour une majorité des étudiants et en une formation accélérée pour les autres.

La bibliothèque [Django](https://www.djangoproject.com/) étant assez conséquente, il n'est pas possible de tout présenter aux étudiants, cependant une assez large portion de ce qui est possible de faire est couverte : création de vue avec le *design pattern* MVT, conception d'un modèle de base de données via l'ORM proposé par Django, maintenance et évolution d'un schéma de base de données au travers des outils de migrations, tests unitaires. Ces concepts, bien qu'enseignés au travers de Django, sont assez généraux et existent dans d'autres bibliothèques.

D'autres bonnes pratiques sont également présentées, comme l'utilisation d'environnements virtuels (virtualenv) afin de cloisonner un projet ou encore le versionnage de code source (git).
